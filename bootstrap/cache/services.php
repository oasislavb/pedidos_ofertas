<?php return array (
  'providers' => 
  array (
    0 => 'Illuminate\\Auth\\AuthServiceProvider',
    1 => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
    2 => 'Illuminate\\Bus\\BusServiceProvider',
    3 => 'Illuminate\\Cache\\CacheServiceProvider',
    4 => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    5 => 'Illuminate\\Cookie\\CookieServiceProvider',
    6 => 'Illuminate\\Database\\DatabaseServiceProvider',
    7 => 'Illuminate\\Encryption\\EncryptionServiceProvider',
    8 => 'Illuminate\\Filesystem\\FilesystemServiceProvider',
    9 => 'Illuminate\\Foundation\\Providers\\FoundationServiceProvider',
    10 => 'Illuminate\\Hashing\\HashServiceProvider',
    11 => 'Illuminate\\Mail\\MailServiceProvider',
    12 => 'Illuminate\\Notifications\\NotificationServiceProvider',
    13 => 'Illuminate\\Pagination\\PaginationServiceProvider',
    14 => 'Illuminate\\Pipeline\\PipelineServiceProvider',
    15 => 'Illuminate\\Queue\\QueueServiceProvider',
    16 => 'Illuminate\\Redis\\RedisServiceProvider',
    17 => 'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider',
    18 => 'Illuminate\\Session\\SessionServiceProvider',
    19 => 'Illuminate\\Validation\\ValidationServiceProvider',
    20 => 'Illuminate\\View\\ViewServiceProvider',
    21 => 'Bedigit\\ReCaptcha\\ReCaptchaServiceProvider',
    22 => 'ChrisKonnertz\\OpenGraph\\OpenGraphServiceProvider',
    23 => 'Creativeorange\\Gravatar\\GravatarServiceProvider',
    24 => 'Cviebrock\\EloquentSluggable\\ServiceProvider',
    25 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    26 => 'Swap\\Laravel\\SwapServiceProvider',
    27 => 'Fruitcake\\Cors\\CorsServiceProvider',
    28 => 'GrahamCampbell\\Flysystem\\FlysystemServiceProvider',
    29 => 'Ignited\\LaravelOmnipay\\LaravelOmnipayServiceProvider',
    30 => 'Intervention\\Image\\ImageServiceProvider',
    31 => 'Jackiedo\\DotenvEditor\\DotenvEditorServiceProvider',
    32 => 'Jaybizzle\\LaravelCrawlerDetect\\LaravelCrawlerDetectServiceProvider',
    33 => 'Jenssegers\\Date\\DateServiceProvider',
    34 => 'Laracasts\\Flash\\FlashServiceProvider',
    35 => 'NotificationChannels\\Twilio\\TwilioProvider',
    36 => 'Illuminate\\Notifications\\NexmoChannelServiceProvider',
    37 => 'Laravel\\Passport\\PassportServiceProvider',
    38 => 'Illuminate\\Notifications\\SlackChannelServiceProvider',
    39 => 'Laravel\\Socialite\\SocialiteServiceProvider',
    40 => 'Laravel\\Tinker\\TinkerServiceProvider',
    41 => 'Collective\\Html\\HtmlServiceProvider',
    42 => 'Mews\\Purifier\\PurifierServiceProvider',
    43 => 'Carbon\\Laravel\\ServiceProvider',
    44 => 'Nexmo\\Laravel\\NexmoServiceProvider',
    45 => 'Prologue\\Alerts\\AlertsServiceProvider',
    46 => 'Propaganistas\\LaravelPhone\\PhoneServiceProvider',
    47 => 'PulkitJalan\\GeoIP\\GeoIPServiceProvider',
    48 => 'Spatie\\Backup\\BackupServiceProvider',
    49 => 'Spatie\\CookieConsent\\CookieConsentServiceProvider',
    50 => 'Spatie\\Permission\\PermissionServiceProvider',
    51 => 'LaravelMandrill\\MandrillServiceProvider',
    52 => 'Torann\\LaravelMetaTags\\MetaTagsServiceProvider',
    53 => 'Unicodeveloper\\DumbPassword\\DumbPasswordServiceProvider',
    54 => 'Unicodeveloper\\Paystack\\PaystackServiceProvider',
    55 => 'Vemcogroup\\SparkPostDriver\\SparkPostDriverServiceProvider',
    56 => 'Watson\\Sitemap\\SitemapServiceProvider',
    57 => 'LarapenIlluminate\\Translation\\TranslationServiceProvider',
    58 => 'App\\Providers\\CachedS3ServiceProvider',
    59 => 'App\\Providers\\DropboxServiceProvider',
    60 => 'App\\Providers\\BackblazeServiceProvider',
    61 => 'App\\Providers\\DigitalOceanServiceProvider',
    62 => 'App\\Providers\\AppServiceProvider',
    63 => 'App\\Providers\\AuthServiceProvider',
    64 => 'App\\Providers\\EventServiceProvider',
    65 => 'App\\Providers\\RouteServiceProvider',
    66 => 'App\\Providers\\PluginsServiceProvider',
    67 => 'Larapen\\TextToImage\\TextToImageServiceProvider',
    68 => 'Laracasts\\Flash\\FlashServiceProvider',
    69 => 'Creativeorange\\Gravatar\\GravatarServiceProvider',
    70 => 'Larapen\\LaravelLocalization\\LaravelLocalizationServiceProvider',
    71 => 'Larapen\\Admin\\AdminServiceProvider',
    72 => 'Larapen\\LaravelDistance\\DistanceServiceProvider',
    73 => 'Prologue\\Alerts\\AlertsServiceProvider',
    74 => 'Spatie\\Backup\\BackupServiceProvider',
    75 => 'NotificationChannels\\Twilio\\TwilioProvider',
    76 => 'Larapen\\Feed\\FeedServiceProvider',
    77 => 'Larapen\\Impersonate\\ImpersonateServiceProvider',
    78 => 'Jackiedo\\DotenvEditor\\DotenvEditorServiceProvider',
    79 => 'Unicodeveloper\\Paystack\\PaystackServiceProvider',
  ),
  'eager' => 
  array (
    0 => 'Illuminate\\Auth\\AuthServiceProvider',
    1 => 'Illuminate\\Cookie\\CookieServiceProvider',
    2 => 'Illuminate\\Database\\DatabaseServiceProvider',
    3 => 'Illuminate\\Encryption\\EncryptionServiceProvider',
    4 => 'Illuminate\\Filesystem\\FilesystemServiceProvider',
    5 => 'Illuminate\\Foundation\\Providers\\FoundationServiceProvider',
    6 => 'Illuminate\\Notifications\\NotificationServiceProvider',
    7 => 'Illuminate\\Pagination\\PaginationServiceProvider',
    8 => 'Illuminate\\Session\\SessionServiceProvider',
    9 => 'Illuminate\\View\\ViewServiceProvider',
    10 => 'Bedigit\\ReCaptcha\\ReCaptchaServiceProvider',
    11 => 'ChrisKonnertz\\OpenGraph\\OpenGraphServiceProvider',
    12 => 'Creativeorange\\Gravatar\\GravatarServiceProvider',
    13 => 'Cviebrock\\EloquentSluggable\\ServiceProvider',
    14 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    15 => 'Swap\\Laravel\\SwapServiceProvider',
    16 => 'Fruitcake\\Cors\\CorsServiceProvider',
    17 => 'GrahamCampbell\\Flysystem\\FlysystemServiceProvider',
    18 => 'Ignited\\LaravelOmnipay\\LaravelOmnipayServiceProvider',
    19 => 'Intervention\\Image\\ImageServiceProvider',
    20 => 'Jackiedo\\DotenvEditor\\DotenvEditorServiceProvider',
    21 => 'Jaybizzle\\LaravelCrawlerDetect\\LaravelCrawlerDetectServiceProvider',
    22 => 'Jenssegers\\Date\\DateServiceProvider',
    23 => 'Laracasts\\Flash\\FlashServiceProvider',
    24 => 'NotificationChannels\\Twilio\\TwilioProvider',
    25 => 'Illuminate\\Notifications\\NexmoChannelServiceProvider',
    26 => 'Laravel\\Passport\\PassportServiceProvider',
    27 => 'Illuminate\\Notifications\\SlackChannelServiceProvider',
    28 => 'Mews\\Purifier\\PurifierServiceProvider',
    29 => 'Carbon\\Laravel\\ServiceProvider',
    30 => 'Nexmo\\Laravel\\NexmoServiceProvider',
    31 => 'Prologue\\Alerts\\AlertsServiceProvider',
    32 => 'Propaganistas\\LaravelPhone\\PhoneServiceProvider',
    33 => 'PulkitJalan\\GeoIP\\GeoIPServiceProvider',
    34 => 'Spatie\\Backup\\BackupServiceProvider',
    35 => 'Spatie\\CookieConsent\\CookieConsentServiceProvider',
    36 => 'Spatie\\Permission\\PermissionServiceProvider',
    37 => 'LaravelMandrill\\MandrillServiceProvider',
    38 => 'Torann\\LaravelMetaTags\\MetaTagsServiceProvider',
    39 => 'Unicodeveloper\\DumbPassword\\DumbPasswordServiceProvider',
    40 => 'Unicodeveloper\\Paystack\\PaystackServiceProvider',
    41 => 'Vemcogroup\\SparkPostDriver\\SparkPostDriverServiceProvider',
    42 => 'Watson\\Sitemap\\SitemapServiceProvider',
    43 => 'App\\Providers\\CachedS3ServiceProvider',
    44 => 'App\\Providers\\DropboxServiceProvider',
    45 => 'App\\Providers\\BackblazeServiceProvider',
    46 => 'App\\Providers\\DigitalOceanServiceProvider',
    47 => 'App\\Providers\\AppServiceProvider',
    48 => 'App\\Providers\\AuthServiceProvider',
    49 => 'App\\Providers\\EventServiceProvider',
    50 => 'App\\Providers\\RouteServiceProvider',
    51 => 'App\\Providers\\PluginsServiceProvider',
    52 => 'Larapen\\TextToImage\\TextToImageServiceProvider',
    53 => 'Laracasts\\Flash\\FlashServiceProvider',
    54 => 'Creativeorange\\Gravatar\\GravatarServiceProvider',
    55 => 'Larapen\\LaravelLocalization\\LaravelLocalizationServiceProvider',
    56 => 'Larapen\\Admin\\AdminServiceProvider',
    57 => 'Larapen\\LaravelDistance\\DistanceServiceProvider',
    58 => 'Prologue\\Alerts\\AlertsServiceProvider',
    59 => 'Spatie\\Backup\\BackupServiceProvider',
    60 => 'NotificationChannels\\Twilio\\TwilioProvider',
    61 => 'Larapen\\Feed\\FeedServiceProvider',
    62 => 'Larapen\\Impersonate\\ImpersonateServiceProvider',
    63 => 'Jackiedo\\DotenvEditor\\DotenvEditorServiceProvider',
    64 => 'Unicodeveloper\\Paystack\\PaystackServiceProvider',
  ),
  'deferred' => 
  array (
    'Illuminate\\Broadcasting\\BroadcastManager' => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
    'Illuminate\\Contracts\\Broadcasting\\Factory' => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
    'Illuminate\\Contracts\\Broadcasting\\Broadcaster' => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
    'Illuminate\\Bus\\Dispatcher' => 'Illuminate\\Bus\\BusServiceProvider',
    'Illuminate\\Contracts\\Bus\\Dispatcher' => 'Illuminate\\Bus\\BusServiceProvider',
    'Illuminate\\Contracts\\Bus\\QueueingDispatcher' => 'Illuminate\\Bus\\BusServiceProvider',
    'cache' => 'Illuminate\\Cache\\CacheServiceProvider',
    'cache.store' => 'Illuminate\\Cache\\CacheServiceProvider',
    'cache.psr6' => 'Illuminate\\Cache\\CacheServiceProvider',
    'memcached.connector' => 'Illuminate\\Cache\\CacheServiceProvider',
    'command.cache.clear' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.cache.forget' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.clear-compiled' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.auth.resets.clear' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.config.cache' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.config.clear' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.db.wipe' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.down' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.environment' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.event.cache' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.event.clear' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.event.list' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.key.generate' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.optimize' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.optimize.clear' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.package.discover' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.failed' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.flush' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.forget' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.listen' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.restart' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.retry' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.work' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.route.cache' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.route.clear' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.route.list' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.seed' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'Illuminate\\Console\\Scheduling\\ScheduleFinishCommand' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'Illuminate\\Console\\Scheduling\\ScheduleRunCommand' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.storage.link' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.up' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.view.cache' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.view.clear' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.cache.table' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.cast.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.channel.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.component.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.console.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.controller.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.event.generate' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.event.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.exception.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.factory.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.job.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.listener.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.mail.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.middleware.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.model.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.notification.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.notification.table' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.observer.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.policy.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.provider.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.failed-table' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.queue.table' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.request.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.resource.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.rule.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.seeder.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.session.table' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.serve' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.stub.publish' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.test.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.vendor.publish' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'migrator' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'migration.repository' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'migration.creator' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate.fresh' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate.install' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate.refresh' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate.reset' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate.rollback' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate.status' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'command.migrate.make' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'composer' => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
    'hash' => 'Illuminate\\Hashing\\HashServiceProvider',
    'hash.driver' => 'Illuminate\\Hashing\\HashServiceProvider',
    'mail.manager' => 'Illuminate\\Mail\\MailServiceProvider',
    'mailer' => 'Illuminate\\Mail\\MailServiceProvider',
    'Illuminate\\Mail\\Markdown' => 'Illuminate\\Mail\\MailServiceProvider',
    'Illuminate\\Contracts\\Pipeline\\Hub' => 'Illuminate\\Pipeline\\PipelineServiceProvider',
    'queue' => 'Illuminate\\Queue\\QueueServiceProvider',
    'queue.worker' => 'Illuminate\\Queue\\QueueServiceProvider',
    'queue.listener' => 'Illuminate\\Queue\\QueueServiceProvider',
    'queue.failer' => 'Illuminate\\Queue\\QueueServiceProvider',
    'queue.connection' => 'Illuminate\\Queue\\QueueServiceProvider',
    'redis' => 'Illuminate\\Redis\\RedisServiceProvider',
    'redis.connection' => 'Illuminate\\Redis\\RedisServiceProvider',
    'auth.password' => 'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider',
    'auth.password.broker' => 'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider',
    'validator' => 'Illuminate\\Validation\\ValidationServiceProvider',
    'validation.presence' => 'Illuminate\\Validation\\ValidationServiceProvider',
    'Laravel\\Socialite\\Contracts\\Factory' => 'Laravel\\Socialite\\SocialiteServiceProvider',
    'command.tinker' => 'Laravel\\Tinker\\TinkerServiceProvider',
    'html' => 'Collective\\Html\\HtmlServiceProvider',
    'form' => 'Collective\\Html\\HtmlServiceProvider',
    'Collective\\Html\\HtmlBuilder' => 'Collective\\Html\\HtmlServiceProvider',
    'Collective\\Html\\FormBuilder' => 'Collective\\Html\\HtmlServiceProvider',
    'translator' => 'LarapenIlluminate\\Translation\\TranslationServiceProvider',
    'translation.loader' => 'LarapenIlluminate\\Translation\\TranslationServiceProvider',
  ),
  'when' => 
  array (
    'Illuminate\\Broadcasting\\BroadcastServiceProvider' => 
    array (
    ),
    'Illuminate\\Bus\\BusServiceProvider' => 
    array (
    ),
    'Illuminate\\Cache\\CacheServiceProvider' => 
    array (
    ),
    'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider' => 
    array (
    ),
    'Illuminate\\Hashing\\HashServiceProvider' => 
    array (
    ),
    'Illuminate\\Mail\\MailServiceProvider' => 
    array (
    ),
    'Illuminate\\Pipeline\\PipelineServiceProvider' => 
    array (
    ),
    'Illuminate\\Queue\\QueueServiceProvider' => 
    array (
    ),
    'Illuminate\\Redis\\RedisServiceProvider' => 
    array (
    ),
    'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider' => 
    array (
    ),
    'Illuminate\\Validation\\ValidationServiceProvider' => 
    array (
    ),
    'Laravel\\Socialite\\SocialiteServiceProvider' => 
    array (
    ),
    'Laravel\\Tinker\\TinkerServiceProvider' => 
    array (
    ),
    'Collective\\Html\\HtmlServiceProvider' => 
    array (
    ),
    'LarapenIlluminate\\Translation\\TranslationServiceProvider' => 
    array (
    ),
  ),
);