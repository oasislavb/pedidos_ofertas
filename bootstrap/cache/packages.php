<?php return array (
  'bedigit/lara-recaptcha' => 
  array (
    'providers' => 
    array (
      0 => 'Bedigit\\ReCaptcha\\ReCaptchaServiceProvider',
    ),
    'aliases' => 
    array (
      'ReCaptcha' => 'Bedigit\\ReCaptcha\\Facades\\ReCaptcha',
    ),
  ),
  'chriskonnertz/open-graph' => 
  array (
    'providers' => 
    array (
      0 => 'ChrisKonnertz\\OpenGraph\\OpenGraphServiceProvider',
    ),
    'aliases' => 
    array (
      'OpenGraph' => 'ChrisKonnertz\\OpenGraph\\OpenGraphFacade',
    ),
  ),
  'creativeorange/gravatar' => 
  array (
    'providers' => 
    array (
      0 => 'Creativeorange\\Gravatar\\GravatarServiceProvider',
    ),
    'aliases' => 
    array (
      'Gravatar' => 'Creativeorange\\Gravatar\\Facades\\Gravatar',
    ),
  ),
  'cviebrock/eloquent-sluggable' => 
  array (
    'providers' => 
    array (
      0 => 'Cviebrock\\EloquentSluggable\\ServiceProvider',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'florianv/laravel-swap' => 
  array (
    'providers' => 
    array (
      0 => 'Swap\\Laravel\\SwapServiceProvider',
    ),
    'aliases' => 
    array (
      'Swap' => 'Swap\\Laravel\\Facades\\Swap',
    ),
  ),
  'fruitcake/laravel-cors' => 
  array (
    'providers' => 
    array (
      0 => 'Fruitcake\\Cors\\CorsServiceProvider',
    ),
  ),
  'graham-campbell/flysystem' => 
  array (
    'providers' => 
    array (
      0 => 'GrahamCampbell\\Flysystem\\FlysystemServiceProvider',
    ),
  ),
  'ignited/laravel-omnipay' => 
  array (
    'providers' => 
    array (
      0 => 'Ignited\\LaravelOmnipay\\LaravelOmnipayServiceProvider',
    ),
    'aliases' => 
    array (
      'Omnipay' => 'Ignited\\LaravelOmnipay\\Facades\\OmnipayFacade',
    ),
  ),
  'intervention/image' => 
  array (
    'providers' => 
    array (
      0 => 'Intervention\\Image\\ImageServiceProvider',
    ),
    'aliases' => 
    array (
      'Image' => 'Intervention\\Image\\Facades\\Image',
    ),
  ),
  'jackiedo/dotenv-editor' => 
  array (
    'providers' => 
    array (
      0 => 'Jackiedo\\DotenvEditor\\DotenvEditorServiceProvider',
    ),
    'aliases' => 
    array (
      'DotenvEditor' => 'Jackiedo\\DotenvEditor\\Facades\\DotenvEditor',
    ),
  ),
  'jaybizzle/laravel-crawler-detect' => 
  array (
    'providers' => 
    array (
      0 => 'Jaybizzle\\LaravelCrawlerDetect\\LaravelCrawlerDetectServiceProvider',
    ),
    'aliases' => 
    array (
      'Crawler' => 'Jaybizzle\\LaravelCrawlerDetect\\Facades\\LaravelCrawlerDetect',
    ),
  ),
  'jenssegers/date' => 
  array (
    'providers' => 
    array (
      0 => 'Jenssegers\\Date\\DateServiceProvider',
    ),
    'aliases' => 
    array (
      'Date' => 'Jenssegers\\Date\\Date',
    ),
  ),
  'laracasts/flash' => 
  array (
    'providers' => 
    array (
      0 => 'Laracasts\\Flash\\FlashServiceProvider',
    ),
    'aliases' => 
    array (
      'Flash' => 'Laracasts\\Flash\\Flash',
    ),
  ),
  'laravel-notification-channels/twilio' => 
  array (
    'providers' => 
    array (
      0 => 'NotificationChannels\\Twilio\\TwilioProvider',
    ),
  ),
  'laravel/nexmo-notification-channel' => 
  array (
    'providers' => 
    array (
      0 => 'Illuminate\\Notifications\\NexmoChannelServiceProvider',
    ),
  ),
  'laravel/passport' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Passport\\PassportServiceProvider',
    ),
  ),
  'laravel/slack-notification-channel' => 
  array (
    'providers' => 
    array (
      0 => 'Illuminate\\Notifications\\SlackChannelServiceProvider',
    ),
  ),
  'laravel/socialite' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Socialite\\SocialiteServiceProvider',
    ),
    'aliases' => 
    array (
      'Socialite' => 'Laravel\\Socialite\\Facades\\Socialite',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'laravelcollective/html' => 
  array (
    'providers' => 
    array (
      0 => 'Collective\\Html\\HtmlServiceProvider',
    ),
    'aliases' => 
    array (
      'Form' => 'Collective\\Html\\FormFacade',
      'Html' => 'Collective\\Html\\HtmlFacade',
    ),
  ),
  'mews/purifier' => 
  array (
    'providers' => 
    array (
      0 => 'Mews\\Purifier\\PurifierServiceProvider',
    ),
    'aliases' => 
    array (
      'Purifier' => 'Mews\\Purifier\\Facades\\Purifier',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'nexmo/laravel' => 
  array (
    'providers' => 
    array (
      0 => 'Nexmo\\Laravel\\NexmoServiceProvider',
    ),
    'aliases' => 
    array (
      'Nexmo' => 'Nexmo\\Laravel\\Facade\\Nexmo',
    ),
  ),
  'prologue/alerts' => 
  array (
    'providers' => 
    array (
      0 => 'Prologue\\Alerts\\AlertsServiceProvider',
    ),
    'aliases' => 
    array (
      'Alert' => 'Prologue\\Alerts\\Facades\\Alert',
    ),
  ),
  'propaganistas/laravel-phone' => 
  array (
    'providers' => 
    array (
      0 => 'Propaganistas\\LaravelPhone\\PhoneServiceProvider',
    ),
  ),
  'pulkitjalan/geoip' => 
  array (
    'providers' => 
    array (
      0 => 'PulkitJalan\\GeoIP\\GeoIPServiceProvider',
    ),
    'aliases' => 
    array (
      'GeoIP' => 'PulkitJalan\\GeoIP\\Facades\\GeoIP',
    ),
  ),
  'spatie/laravel-backup' => 
  array (
    'providers' => 
    array (
      0 => 'Spatie\\Backup\\BackupServiceProvider',
    ),
  ),
  'spatie/laravel-cookie-consent' => 
  array (
    'providers' => 
    array (
      0 => 'Spatie\\CookieConsent\\CookieConsentServiceProvider',
    ),
  ),
  'spatie/laravel-permission' => 
  array (
    'providers' => 
    array (
      0 => 'Spatie\\Permission\\PermissionServiceProvider',
    ),
  ),
  'therobfonz/laravel-mandrill-driver' => 
  array (
    'providers' => 
    array (
      0 => 'LaravelMandrill\\MandrillServiceProvider',
    ),
  ),
  'torann/laravel-meta-tags' => 
  array (
    'providers' => 
    array (
      0 => 'Torann\\LaravelMetaTags\\MetaTagsServiceProvider',
    ),
    'aliases' => 
    array (
      'MetaTag' => 'Torann\\LaravelMetaTags\\Facades\\MetaTag',
    ),
  ),
  'unicodeveloper/laravel-password' => 
  array (
    'providers' => 
    array (
      0 => 'Unicodeveloper\\DumbPassword\\DumbPasswordServiceProvider',
    ),
  ),
  'unicodeveloper/laravel-paystack' => 
  array (
    'providers' => 
    array (
      0 => 'Unicodeveloper\\Paystack\\PaystackServiceProvider',
    ),
    'aliases' => 
    array (
      'Paystack' => 'Unicodeveloper\\Paystack\\Facades\\Paystack',
    ),
  ),
  'vemcogroup/laravel-sparkpost-driver' => 
  array (
    'providers' => 
    array (
      0 => 'Vemcogroup\\SparkPostDriver\\SparkPostDriverServiceProvider',
    ),
  ),
  'watson/sitemap' => 
  array (
    'providers' => 
    array (
      0 => 'Watson\\Sitemap\\SitemapServiceProvider',
    ),
    'aliases' => 
    array (
      'Sitemap' => 'Watson\\Sitemap\\Facades\\Sitemap',
    ),
  ),
);