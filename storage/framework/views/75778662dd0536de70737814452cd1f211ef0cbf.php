
<?php if(config('settings.single.simditor_wysiwyg')): ?>
    <link media="all" rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/plugins/simditor/styles/simditor.css')); ?>" />
    <?php if(config('lang.direction') == 'rtl'): ?>
        <link media="all" rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/plugins/simditor/styles/simditor-rtl.css')); ?>" />
    <?php endif; ?>
<?php endif; ?>



<?php if(!config('settings.single.simditor_wysiwyg') && config('settings.single.ckeditor_wysiwyg')): ?>
    
<?php endif; ?>
<?php /**PATH /var/www/vhosts/pedidosmovil.com/clasificados1.pedidosmovil.com/resources/views/layouts/inc/tools/wysiwyg/css.blade.php ENDPATH**/ ?>