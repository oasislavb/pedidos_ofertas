<?php $__env->startSection('wizard'); ?>
	<?php echo $__env->make('post.createOrEdit.multiSteps.inc.wizard', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('common.spacer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<div class="main-container">
		<div class="container">
			<div class="row">
				
				<?php echo $__env->make('post.inc.notification', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

				<div class="col-md-9 page-content">
					<div class="inner-box category-content">
						<h2 class="title-2">
							<strong><i class="icon-docs"></i> <?php echo e(t('Post Free Ads')); ?></strong>
						</h2>
						
						<div class="row">
							<div class="col-xl-12">
								
								<form class="form-horizontal" id="postForm" method="POST" action="<?php echo e(url()->current()); ?>" enctype="multipart/form-data">
									<?php echo csrf_field(); ?>

									<fieldset>

										<!-- parent_id -->
										<?php $parentIdError = (isset($errors) and $errors->has('category_id')) ? ' is-invalid' : ''; ?>
										<div class="form-group row required">
											<label class="col-md-3 col-form-label<?php echo e($parentIdError); ?>"><?php echo e(t('Category')); ?> <sup>*</sup></label>
											<div class="col-md-8">
												<select name="parent_id" id="parentId" class="form-control selecter<?php echo e($parentIdError); ?>">
													<option value="0" data-type=""
															<?php if(old('parent_id')=='' or old('parent_id')==0): ?>
																selected="selected"
															<?php endif; ?>
													> <?php echo e(t('Select a category')); ?> </option>
													<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
														<option value="<?php echo e($cat->tid); ?>" data-type="<?php echo e($cat->type); ?>"
																<?php if(old('parent_id')==$cat->tid): ?>
																	selected="selected"
																<?php endif; ?>
														> <?php echo e($cat->name); ?> </option>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</select>
												<input type="hidden" name="parent_type" id="parentType" value="<?php echo e(old('parent_type')); ?>">
											</div>
										</div>

										<!-- category_id -->
										<?php $categoryIdError = (isset($errors) and $errors->has('category_id')) ? ' is-invalid' : ''; ?>
										<div id="subCatBloc" class="form-group row required">
											<label class="col-md-3 col-form-label<?php echo e($categoryIdError); ?>"><?php echo e(t('Sub-Category')); ?> <sup>*</sup></label>
											<div class="col-md-8">
												<select name="category_id" id="categoryId" class="form-control selecter<?php echo e($categoryIdError); ?>">
													<option value="0" data-type=""
															<?php if(old('category_id')=='' or old('category_id')==0): ?>
																selected="selected"
															<?php endif; ?>
													> <?php echo e(t('Select a sub-category')); ?> </option>
												</select>
												<input type="hidden" name="category_type" id="categoryType" value="<?php echo e(old('category_type')); ?>">
											</div>
										</div>

										<!-- post_type_id -->
										<?php $postTypeIdError = (isset($errors) and $errors->has('post_type_id')) ? ' is-invalid' : ''; ?>
										<div id="postTypeBloc" class="form-group row required">
											<label class="col-md-3 col-form-label"><?php echo e(t('Type')); ?> <sup>*</sup></label>
											<div class="col-md-8">
												<?php $__currentLoopData = $postTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $postType): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<div class="form-check form-check-inline pt-2">
													<input name="post_type_id"
														   id="postTypeId-<?php echo e($postType->tid); ?>"
														   value="<?php echo e($postType->tid); ?>"
														   type="radio"
														   class="form-check-input<?php echo e($postTypeIdError); ?>" <?php echo e((old('post_type_id')==$postType->tid) ? 'checked="checked"' : ''); ?>

													>
													<label class="form-check-label" for="postTypeId-<?php echo e($postType->tid); ?>">
														<?php echo e($postType->name); ?>

													</label>
												</div>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</div>
										</div>

										<!-- title -->
										<?php $titleError = (isset($errors) and $errors->has('title')) ? ' is-invalid' : ''; ?>
										<div class="form-group row required">
											<label class="col-md-3 col-form-label" for="title"><?php echo e(t('Title')); ?> <sup>*</sup></label>
											<div class="col-md-8">
												<input id="title" name="title" placeholder="<?php echo e(t('Ad title')); ?>" class="form-control input-md<?php echo e($titleError); ?>"
													   type="text" value="<?php echo e(old('title')); ?>">
												<small id="" class="form-text text-muted"><?php echo e(t('A great title needs at least 60 characters.')); ?></small>
											</div>
										</div>

										<!-- description -->
										<?php $descriptionError = (isset($errors) and $errors->has('description')) ? ' is-invalid' : ''; ?>
										<div class="form-group row required">
											<?php
												$descriptionErrorLabel = '';
												$descriptionColClass = 'col-md-8';
												if (config('settings.single.simditor_wysiwyg')) {
													$descriptionColClass = 'col-md-12';
													$descriptionErrorLabel = $descriptionError;
												}
											?>
											<label class="col-md-3 col-form-label<?php echo e($descriptionErrorLabel); ?>" for="description">
												<?php echo e(t('Description')); ?> <sup>*</sup>
											</label>
											<div class="<?php echo e($descriptionColClass); ?>">
												<textarea class="form-control<?php echo e($descriptionError); ?>"
														  id="description"
														  name="description"
														  rows="10"
												><?php echo e(old('description')); ?></textarea>
												<small id="" class="form-text text-muted"><?php echo e(t('Describe what makes your ad unique')); ?>...</small>
											</div>
										</div>
										
										<!-- customFields -->
										<div id="customFields"></div>

										<!-- price -->
										<?php $priceError = (isset($errors) and $errors->has('price')) ? ' is-invalid' : ''; ?>
										<div id="priceBloc" class="form-group row">
											<label class="col-md-3 col-form-label" for="price"><?php echo e(t('Price')); ?></label>
											<div class="input-group col-md-8">
												<div class="input-group-prepend">
													<span class="input-group-text"><?php echo config('currency')['symbol']; ?></span>
												</div>
												
												<input id="price"
													   name="price"
													   class="form-control<?php echo e($priceError); ?>"
													   placeholder="<?php echo e(t('e.i. 15000')); ?>"
													   type="text" value="<?php echo e(old('price')); ?>"
												>
												
												<div class="input-group-append">
													<span class="input-group-text">
														<input id="negotiable" name="negotiable" type="checkbox"
															   value="1" <?php echo e((old('negotiable')=='1') ? 'checked="checked"' : ''); ?>>&nbsp;<small><?php echo e(t('Negotiable')); ?></small>
													</span>
												</div>
											</div>
										</div>
										
										<!-- country_code -->
										<?php $countryCodeError = (isset($errors) and $errors->has('country_code')) ? ' is-invalid' : ''; ?>
										<?php if(empty(config('country.code'))): ?>
											<div class="form-group row required">
												<label class="col-md-3 col-form-label<?php echo e($countryCodeError); ?>" for="country_code"><?php echo e(t('Your Country')); ?> <sup>*</sup></label>
												<div class="col-md-8">
													<select id="countryCode" name="country_code" class="form-control sselecter<?php echo e($countryCodeError); ?>">
														<option value="0" <?php echo e((!old('country_code') or old('country_code')==0) ? 'selected="selected"' : ''); ?>> <?php echo e(t('Select a country')); ?> </option>
														<?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value="<?php echo e($item->get('code')); ?>" <?php echo e((old('country_code', (!empty(config('ipCountry.code'))) ? config('ipCountry.code') : 0)==$item->get('code')) ? 'selected="selected"' : ''); ?>><?php echo e($item->get('name')); ?></option>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													</select>
												</div>
											</div>
										<?php else: ?>
											<input id="countryCode" name="country_code" type="hidden" value="<?php echo e(config('country.code')); ?>">
										<?php endif; ?>
										
										<?php
										/*
										@if (\Illuminate\Support\Facades\Schema::hasColumn('posts', 'address'))
										<!-- address -->
										<div class="form-group required <?php echo ($errors->has('address')) ? ' is-invalid' : ''; ?>">
											<label class="col-md-3 control-label" for="title">{{ t('Address') }} </label>
											<div class="col-md-8">
												<input id="address" name="address" placeholder="{{ t('Address') }}" class="form-control input-md"
													   type="text" value="{{ old('address') }}">
												<span class="help-block">{{ t('Fill an address to display on Google Maps.') }} </span>
											</div>
										</div>
										@endif
										*/
										?>
										
										<?php if(config('country.admin_field_active') == 1 and in_array(config('country.admin_type'), ['1', '2'])): ?>
											<!-- admin_code -->
											<?php $adminCodeError = (isset($errors) and $errors->has('admin_code')) ? ' is-invalid' : ''; ?>
											<div id="locationBox" class="form-group row required">
												<label class="col-md-3 col-form-label<?php echo e($adminCodeError); ?>" for="admin_code"><?php echo e(t('Location')); ?> <sup>*</sup></label>
												<div class="col-md-8">
													<select id="adminCode" name="admin_code" class="form-control sselecter<?php echo e($adminCodeError); ?>">
														<option value="0" <?php echo e((!old('admin_code') or old('admin_code')==0) ? 'selected="selected"' : ''); ?>>
															<?php echo e(t('Select your Location')); ?>

														</option>
													</select>
												</div>
											</div>
										<?php endif; ?>
									
										<!-- city_id -->
										<?php $cityIdError = (isset($errors) and $errors->has('city_id')) ? ' is-invalid' : ''; ?>
										<div id="cityBox" class="form-group row required">
											<label class="col-md-3 col-form-label<?php echo e($cityIdError); ?>" for="city_id"><?php echo e(t('City')); ?> <sup>*</sup></label>
											<div class="col-md-8">
												<select id="cityId" name="city_id" class="form-control sselecter<?php echo e($cityIdError); ?>">
													<option value="0" <?php echo e((!old('city_id') or old('city_id')==0) ? 'selected="selected"' : ''); ?>>
														<?php echo e(t('Select a city')); ?>

													</option>
												</select>
											</div>
										</div>
										
										<!-- tags -->
										<?php $tagsError = (isset($errors) and $errors->has('tags')) ? ' is-invalid' : ''; ?>
										<div class="form-group row">
											<label class="col-md-3 col-form-label" for="tags"><?php echo e(t('Tags')); ?></label>
											<div class="col-md-8">
												<input id="tags"
													   name="tags"
													   placeholder="<?php echo e(t('Tags')); ?>"
													   class="form-control input-md<?php echo e($tagsError); ?>"
													   type="text"
													   value="<?php echo e(old('tags')); ?>"
												>
												<small id="" class="form-text text-muted"><?php echo e(t('Enter the tags separated by commas.')); ?></small>
											</div>
										</div>
										
										
										<div class="content-subheading">
											<i class="icon-user fa"></i>
											<strong><?php echo e(t('Seller information')); ?></strong>
										</div>
										
										
										<!-- contact_name -->
										<?php $contactNameError = (isset($errors) and $errors->has('contact_name')) ? ' is-invalid' : ''; ?>
										<?php if(auth()->check()): ?>
											<input id="contact_name" name="contact_name" type="hidden" value="<?php echo e(auth()->user()->name); ?>">
										<?php else: ?>
											<div class="form-group row required">
												<label class="col-md-3 col-form-label" for="contact_name"><?php echo e(t('Your name')); ?> <sup>*</sup></label>
												<div class="col-md-8">
													<input id="contact_name" name="contact_name" placeholder="<?php echo e(t('Your name')); ?>"
														   class="form-control input-md<?php echo e($contactNameError); ?>" type="text" value="<?php echo e(old('contact_name')); ?>">
												</div>
											</div>
										<?php endif; ?>
									
										<!-- email -->
										<?php $emailError = (isset($errors) and $errors->has('email')) ? ' is-invalid' : ''; ?>
										<div class="form-group row required">
											<label class="col-md-3 col-form-label" for="email"> <?php echo e(t('Email')); ?> </label>
											<div class="input-group col-md-8">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="icon-mail"></i></span>
												</div>
												
												<input id="email" name="email"
													   class="form-control<?php echo e($emailError); ?>" placeholder="<?php echo e(t('Email')); ?>" type="text"
													   value="<?php echo e(old('email', ((auth()->check() and isset(auth()->user()->email)) ? auth()->user()->email : ''))); ?>">
											</div>
										</div>
										
										<?php
											if (auth()->check()) {
												$formPhone = (auth()->user()->country_code == config('country.code')) ? auth()->user()->phone : '';
											} else {
												$formPhone = '';
											}
										?>
										<!-- phone -->
										<?php $phoneError = (isset($errors) and $errors->has('phone')) ? ' is-invalid' : ''; ?>
										<div class="form-group row required">
											<label class="col-md-3 col-form-label" for="phone"><?php echo e(t('Phone Number')); ?></label>
											<div class="input-group col-md-8">
												<div class="input-group-prepend">
													<span id="phoneCountry" class="input-group-text"><?php echo getPhoneIcon(config('country.code')); ?></span>
												</div>
												
												<input id="phone" name="phone"
													   placeholder="<?php echo e(t('Phone Number')); ?>"
													   class="form-control input-md<?php echo e($phoneError); ?>" type="text"
													   value="<?php echo e(phoneFormat(old('phone', $formPhone), old('country', config('country.code')))); ?>"
												>
												
												<div class="input-group-append">
													<span class="input-group-text">
														<input name="phone_hidden" id="phoneHidden" type="checkbox"
															   value="1" <?php echo e((old('phone_hidden')=='1') ? 'checked="checked"' : ''); ?>>&nbsp;<small><?php echo e(t('Hide')); ?></small>
													</span>
												</div>
											</div>
										</div>
										
										<?php if(!auth()->check()): ?>
											<?php if(in_array(config('settings.single.auto_registration'), [1, 2])): ?>
												<!-- auto_registration -->
												<?php if(config('settings.single.auto_registration') == 1): ?>
													<?php $autoRegistrationError = (isset($errors) and $errors->has('auto_registration')) ? ' is-invalid' : ''; ?>
													<div class="form-group row required">
														<label class="col-md-3 col-form-label"></label>
														<div class="col-md-8">
															<div class="form-check">
																<input name="auto_registration" id="auto_registration"
																	   class="form-check-input<?php echo e($autoRegistrationError); ?>"
																	   value="1"
																	   type="checkbox"
																	   checked="checked"
																>
																
																<label class="form-check-label" for="auto_registration">
																	<?php echo t('I want to register by submitting this ad.'); ?>

																</label>
															</div>
															<small id="" class="form-text text-muted"><?php echo e(t('You will receive your authentication information by email.')); ?></small>
															<div style="clear:both"></div>
														</div>
													</div>
												<?php else: ?>
													<input type="hidden" name="auto_registration" id="auto_registration" value="1">
												<?php endif; ?>
											<?php endif; ?>
										<?php endif; ?>
										
										<?php echo $__env->make('layouts.inc.tools.recaptcha', ['colLeft' => 'col-md-3', 'colRight' => 'col-md-8'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

										<!-- term -->
										<?php $termError = (isset($errors) and $errors->has('term')) ? ' is-invalid' : ''; ?>
										<div class="form-group row required">
											<label class="col-md-3 col-form-label<?php echo e($termError); ?>"></label>
											<div class="col-md-8">
												<label class="checkbox mb-0" for="term-0">
													<?php echo t('By continuing on this website, you accept our <a :attributes>Terms of Use</a>', ['attributes' => getUrlPageByType('terms')]); ?>

												</label>
											</div>
										</div>

										<!-- Button  -->
										<div class="form-group row pt-3">
											<div class="col-md-12 text-center">
												<button id="nextStepBtn" class="btn btn-primary btn-lg"> <?php echo e(t('Submit')); ?> </button>
											</div>
										</div>

									</fieldset>
								</form>

							</div>
						</div>
					</div>
				</div>
				<!-- /.page-content -->

				<div class="col-md-3 reg-sidebar">
					<div class="reg-sidebar-inner text-center">
						<div class="promo-text-box"><i class=" icon-picture fa fa-4x icon-color-1"></i>
							<h3><strong><?php echo e(t('Post Free Ads')); ?></strong></h3>
							<p>
								<?php echo e(t('Do you have something to sell, to rent, any service to offer or a job offer? Post it at :app_name, its free, local, easy, reliable and super fast!', ['app_name' => config('app.name')])); ?>

							</p>
						</div>

						<div class="card sidebar-card">
							<div class="card-header uppercase">
								<small><strong><?php echo e(t('How to sell quickly?')); ?></strong></small>
							</div>
							<div class="card-content">
								<div class="card-body text-left">
									<ul class="list-check">
										<li> <?php echo e(t('Use a brief title and description of the item')); ?> </li>
										<li> <?php echo e(t('Make sure you post in the correct category')); ?></li>
										<li> <?php echo e(t('Add nice photos to your ad')); ?></li>
										<li> <?php echo e(t('Put a reasonable price')); ?></li>
										<li> <?php echo e(t('Check the item before publish')); ?></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_styles'); ?>
	<?php echo $__env->make('layouts.inc.tools.wysiwyg.css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
    <?php echo $__env->make('layouts.inc.tools.wysiwyg.js', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.2.3/jquery.payment.min.js"></script>
	<?php if(file_exists(public_path() . '/assets/plugins/forms/validation/localization/messages_'.config('app.locale').'.min.js')): ?>
		<script src="<?php echo e(url('assets/plugins/forms/validation/localization/messages_'.config('app.locale').'.min.js')); ?>" type="text/javascript"></script>
	<?php endif; ?>
	
	<script>
		/* Translation */
		var lang = {
			'select': {
				'category': "<?php echo e(t('Select a category')); ?>",
				'subCategory': "<?php echo e(t('Select a sub-category')); ?>",
				'country': "<?php echo e(t('Select a country')); ?>",
				'admin': "<?php echo e(t('Select a location')); ?>",
				'city': "<?php echo e(t('Select a city')); ?>"
			},
			'price': "<?php echo e(t('Price')); ?>",
			'salary': "<?php echo e(t('Salary')); ?>",
			'nextStepBtnLabel': {
			    'next': "<?php echo e(t('Next')); ?>",
                'submit': "<?php echo e(t('Submit')); ?>"
			}
		};
		
		/* Categories */
		var category = <?php echo e(old('parent_id', 0)); ?>;
		var categoryType = '<?php echo e(old('parent_type')); ?>';
		if (categoryType=='') {
			var selectedCat = $('select[name=parent_id]').find('option:selected');
			categoryType = selectedCat.data('type');
		}
		var subCategory = <?php echo e(old('category_id', 0)); ?>;
		
		/* Custom Fields */
		var errors = '<?php echo addslashes($errors->toJson()); ?>';
		var oldInput = '<?php echo addslashes(collect(session()->getOldInput('cf'))->toJson()); ?>';
		var postId = '';
		
		/* Locations */
        var countryCode = '<?php echo e(old('country_code', config('country.code', 0))); ?>';
        var adminType = '<?php echo e(config('country.admin_type', 0)); ?>';
        var selectedAdminCode = '<?php echo e(old('admin_code', (isset($admin) ? $admin->code : 0))); ?>';
        var cityId = '<?php echo e(old('city_id', (isset($post) ? $post->city_id : 0))); ?>';
		
		/* Packages */
		var packageIsEnabled = false;
        <?php if(isset($packages) and isset($paymentMethods) and $packages->count() > 0 and $paymentMethods->count() > 0): ?>
            packageIsEnabled = true;
        <?php endif; ?>

	</script>
	<script>
		$(document).ready(function() {
			$('#tags').tagit({
				fieldName: 'tags',
				placeholderText: '<?php echo e(t('add a tag')); ?>',
				caseSensitive: false,
				allowDuplicates: false,
				allowSpaces: false,
				tagLimit: <?php echo e((int)config('settings.single.tags_limit', 15)); ?>,
				singleFieldDelimiter: ','
			});
		});
	</script>
	
	<script src="<?php echo e(url('assets/js/app/d.select.category.js') . vTime()); ?>"></script>
	<script src="<?php echo e(url('assets/js/app/d.select.location.js') . vTime()); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/vhosts/pedidosmovil.com/ofertas.pedidosmovil.com/resources/views/post/createOrEdit/multiSteps/create.blade.php ENDPATH**/ ?>