<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('common.spacer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<div class="main-container">
		<div class="container">
			<div class="row clearfix">
				
				<?php if(isset($errors) and $errors->any()): ?>
					<div class="col-md-12">
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h5><strong><?php echo e(t('Oops ! An error has occurred. Please correct the red fields in the form')); ?></strong></h5>
							<ul class="list list-check">
								<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<li><?php echo e($error); ?></li>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</ul>
						</div>
					</div>
				<?php endif; ?>
					
				<div class="col-md-12">
					<div class="contact-form">
						
						<h3 class="gray mt-0">
							<strong><a href="<?php echo e(\App\Helpers\UrlGen::post($post)); ?>"><?php echo e($title); ?></a></strong>
						</h3>
						<hr class="mt-1">
						<h4><?php echo e(t('There\'s something wrong with this ad?')); ?></h4>
		
						<form role="form" method="POST" action="<?php echo e(lurl('posts/' . $post->id . '/report')); ?>">
							<?php echo csrf_field(); ?>

							<fieldset>
								<!-- report_type_id -->
								<?php $reportTypeIdError = (isset($errors) and $errors->has('report_type_id')) ? ' is-invalid' : ''; ?>
								<div class="form-group required">
									<label for="report_type_id" class="control-label<?php echo e($reportTypeIdError); ?>"><?php echo e(t('Reason')); ?> <sup>*</sup></label>
									<select id="reportTypeId" name="report_type_id" class="form-control selecter<?php echo e($reportTypeIdError); ?>">
										<option value=""><?php echo e(t('Select a reason')); ?></option>
										<?php $__currentLoopData = $reportTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reportType): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($reportType->id); ?>" <?php echo e((old('report_type_id', 0)==$reportType->id) ? 'selected="selected"' : ''); ?>>
												<?php echo e($reportType->name); ?>

											</option>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</select>
								</div>
								
								<!-- email -->
								<?php if(auth()->check() and isset(auth()->user()->email)): ?>
									<input type="hidden" name="email" value="<?php echo e(auth()->user()->email); ?>">
								<?php else: ?>
									<?php $emailError = (isset($errors) and $errors->has('email')) ? ' is-invalid' : ''; ?>
									<div class="form-group required">
										<label for="email" class="control-label"><?php echo e(t('Your E-mail')); ?> <sup>*</sup></label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="icon-mail"></i></span>
											</div>
											<input id="email" name="email" type="text" maxlength="60" class="form-control<?php echo e($emailError); ?>" value="<?php echo e(old('email')); ?>">
										</div>
									</div>
								<?php endif; ?>
							
								<!-- message -->
								<?php $messageError = (isset($errors) and $errors->has('message')) ? ' is-invalid' : ''; ?>
								<div class="form-group required">
									<label for="message" class="control-label"><?php echo e(t('Message')); ?> <sup>*</sup> <span class="text-count"></span></label>
									<textarea id="message" name="message" class="form-control<?php echo e($messageError); ?>" rows="10"><?php echo e(old('message')); ?></textarea>
								</div>
								
								<?php echo $__env->make('layouts.inc.tools.recaptcha', ['label' => true], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			
								<input type="hidden" name="post_id" value="<?php echo e($post->id); ?>">
								<input type="hidden" name="abuseForm" value="1">
								
								<div class="form-group">
									<a href="<?php echo e(rawurldecode(URL::previous())); ?>" class="btn btn-default btn-lg"><?php echo e(t('Back')); ?></a>
									<button type="submit" class="btn btn-primary btn-lg"><?php echo e(t('Send Report')); ?></button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
				
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
	<script src="<?php echo e(url('assets/js/form-validation.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/vhosts/pedidosmovil.com/ofertas.pedidosmovil.com/resources/views/post/report.blade.php ENDPATH**/ ?>