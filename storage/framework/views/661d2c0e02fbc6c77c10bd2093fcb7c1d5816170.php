<?php if($xPanel->hasAccess('update')): ?>
	<a href="<?php echo e(url($xPanel->route.'/'.$entry->getKey().'/edit')); ?>" class="btn btn-xs btn-primary">
		<i class="fa fa-edit"></i> <?php echo e(trans('admin::messages.edit')); ?>

    </a>
<?php endif; ?><?php /**PATH /var/www/vhosts/pedidosmovil.com/clasificados1.pedidosmovil.com/resources/views/vendor/admin/panel/buttons/update.blade.php ENDPATH**/ ?>