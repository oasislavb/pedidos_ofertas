<?php $__env->startSection('search'); ?>
	##parent-placeholder-3559d7accf00360971961ca18989adc0614089c0##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('common.spacer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<div class="main-container inner-page">
		<div class="container">
			<div class="section-content">
				<div class="row">

					<?php if(Session::has('message')): ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo e(session('message')); ?>

						</div>
					<?php endif; ?>

					<?php if(Session::has('flash_notification')): ?>
						<div class="col-xl-12">
							<div class="row">
								<div class="col-xl-12">
									<?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
								</div>
							</div>
						</div>
					<?php endif; ?>
					
					<?php echo $__env->make('home.inc.spacer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
					<h1 class="text-center title-1"><strong><?php echo e(t('Sitemap')); ?></strong></h1>
					<hr class="center-block small mt-0">
						
					<div class="col-xl-12">
						<div class="content-box">
							<div class="row-featured-category">
								<div class="col-xl-12 box-title">
									<h2>
										<span class="title-3" style="font-weight: bold;"><?php echo e(t('List of Categories and Sub-categories')); ?></span>
									</h2>
								</div>
								
								<div class="col-xl-12">
									<div class="list-categories-children styled">
										<div class="row">
											<?php $__currentLoopData = $cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $col): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<div class="col-md-4 col-sm-4 <?php echo e((count($cats) == $key+1) ? 'last-column' : ''); ?>">
													<?php $__currentLoopData = $col; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $iCat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
														
														<?php
															$randomId = '-' . substr(uniqid(rand(), true), 5, 5);
														?>
														
														<div class="cat-list">
															<h3 class="cat-title rounded">
																<a href="<?php echo e(\App\Helpers\UrlGen::category($iCat)); ?>">
																	<i class="<?php echo e($iCat->icon_class ?? 'icon-ok'); ?>"></i>
																	<?php echo e($iCat->name); ?> <span class="count"></span>
																</a>
																<?php if(isset($subCats) and $subCats->has($iCat->tid)): ?>
																	<span class="btn-cat-collapsed collapsed"
																		  data-toggle="collapse"
																		  data-target=".cat-id-<?php echo e($iCat->id . $randomId); ?>"
																		  aria-expanded="false"
																	>
																		<span class="icon-down-open-big"></span>
																	</span>
																<?php endif; ?>
															</h3>
															<ul class="cat-collapse collapse show cat-id-<?php echo e($iCat->id . $randomId); ?> long-list-home">
																<?php if(isset($subCats) and $subCats->has($iCat->tid)): ?>
																	<?php $__currentLoopData = $subCats->get($iCat->tid); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $iSubCat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																		<li>
																			<a href="<?php echo e(\App\Helpers\UrlGen::category($iSubCat, 1)); ?>">
																				<?php echo e($iSubCat->name); ?>

																			</a>
																		</li>
																	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																<?php endif; ?>
															</ul>
														</div>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</div>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<?php if(isset($cities)): ?>
						<?php echo $__env->make('home.inc.spacer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
						<div class="col-xl-12">
							<div class="content-box mb-0">
								<div class="row-featured-category">
									<div class="col-xl-12 box-title">
										<div class="inner">
											<h2>
												<span class="title-3" style="font-weight: bold;">
													<i class="icon-location-2"></i> <?php echo e(t('List of Cities in')); ?> <?php echo e(config('country.name')); ?>

												</span>
											</h2>
										</div>
									</div>
									
									<div class="col-xl-12">
										<div class="list-categories-children">
											<div class="row">
												<?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cols): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<ul class="cat-list col-lg-3 col-md-4 col-sm-6 <?php echo e(($cities->count() == $key+1) ? 'cat-list-border' : ''); ?>">
														<?php $__currentLoopData = $cols; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $j => $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<li>
																<a href="<?php echo e(\App\Helpers\UrlGen::city($city)); ?>" title="<?php echo e(t('Free Ads')); ?> <?php echo e($city->name); ?>">
																	<strong><?php echo e($city->name); ?></strong>
																</a>
															</li>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													</ul>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>

				</div>
				
				<?php echo $__env->make('layouts.inc.social.horizontal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
				
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('before_scripts'); ?>
	##parent-placeholder-094e37d5f5003ce853bb823b74f26393141d779d##
	<script>
		var maxSubCats = 15;
	</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/vhosts/pedidosmovil.com/ofertas.pedidosmovil.com/resources/views/sitemap/index.blade.php ENDPATH**/ ?>