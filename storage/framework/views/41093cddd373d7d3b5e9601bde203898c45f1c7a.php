<?php $__env->startSection('content'); ?>
	<?php if(!(isset($paddingTopExists) and $paddingTopExists)): ?>
		<div class="h-spacer"></div>
	<?php endif; ?>
	<div class="main-container">
		<div class="container">
			<div class="row">

				<?php if(isset($errors) and $errors->any()): ?>
					<div class="col-xl-12">
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<ul class="list list-check">
								<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<li><?php echo e($error); ?></li>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</ul>
						</div>
					</div>
				<?php endif; ?>

				<?php if(session('status')): ?>
					<div class="col-xl-12">
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<p><?php echo e(session('status')); ?></p>
						</div>
					</div>
				<?php endif; ?>

				<?php if(session('email')): ?>
					<div class="col-xl-12">
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<p><?php echo e(session('email')); ?></p>
						</div>
					</div>
				<?php endif; ?>
					
				<?php if(session('phone')): ?>
					<div class="col-xl-12">
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<p><?php echo e(session('phone')); ?></p>
						</div>
					</div>
				<?php endif; ?>
					
				<?php if(session('login')): ?>
					<div class="col-xl-12">
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<p><?php echo e(session('login')); ?></p>
						</div>
					</div>
				<?php endif; ?>

				<?php if(Session::has('flash_notification')): ?>
					<div class="col-xl-12">
						<div class="row">
							<div class="col-xl-12">
								<?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<div class="col-lg-5 col-md-8 col-sm-10 col-xs-12 login-box">
					<div class="card card-default">
						<div class="panel-intro text-center">
							<h2 class="logo-title">
								<span class="logo-icon"> </span> <?php echo e(t('Password')); ?> <span> </span>
							</h2>
						</div>
						
						<div class="card-body">
							<form id="pwdForm" role="form" method="POST" action="<?php echo e(lurl('password/email')); ?>">
								<?php echo csrf_field(); ?>

								
								<!-- login -->
								<?php $loginError = (isset($errors) and $errors->has('login')) ? ' is-invalid' : ''; ?>
								<div class="form-group">
									<label for="login" class="col-form-label"><?php echo e(t('Login') . ' (' . getLoginLabel() . ')'); ?>:</label>
									<div class="input-icon">
										<i class="icon-user fa"></i>
										<input id="login"
											   name="login"
											   type="text"
											   placeholder="<?php echo e(getLoginLabel()); ?>"
											   class="form-control<?php echo e($loginError); ?>"
											   value="<?php echo e(old('login')); ?>"
										>
									</div>
								</div>
								
								<?php echo $__env->make('layouts.inc.tools.recaptcha', ['noLabel' => true], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
								
								<!-- Submit -->
								<div class="form-group">
									<button id="pwdBtn" type="submit" class="btn btn-primary btn-lg btn-block"><?php echo e(t('Submit')); ?></button>
								</div>
							</form>
						</div>
						
						<div class="card-footer text-center">
							<a href="<?php echo e(lurl(trans('routes.login'))); ?>"> <?php echo e(t('Back to the Log In page')); ?> </a>
						</div>
					</div>
					<div class="login-box-btm text-center">
						<p>
							<?php echo e(t('Don\'t have an account?')); ?> <br>
							<a href="<?php echo e(lurl(trans('routes.register'))); ?>"><strong><?php echo e(t('Sign Up !')); ?></strong></a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('after_scripts'); ?>
	<script>
		$(document).ready(function () {
			$("#pwdBtn").click(function () {
				$("#pwdForm").submit();
				return false;
			});
		});
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/vhosts/pedidosmovil.com/ofertas.pedidosmovil.com/resources/views/auth/passwords/email.blade.php ENDPATH**/ ?>