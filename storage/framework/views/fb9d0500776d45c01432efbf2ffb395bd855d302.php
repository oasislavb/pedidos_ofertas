<?php
if (!isset($cats)) {
    $cats = collect([]);
}

$cats = $cats->groupBy('parent_id');
$subCats = $cats;
if ($cats->has(0)) {
	$cats = $cats->get(0);
}
if ($subCats->has(0)) {
	$subCats = $subCats->forget(0);
}
?>
<?php
	if (
		(isset($subCats) and !empty($subCats) and isset($cat) and !empty($cat) and $subCats->has($cat->tid)) ||
		(isset($cats) and !empty($cats))
	):
?>
<?php if(isset($subCats) and !empty($subCats) and isset($cat) and !empty($cat)): ?>
	<?php if($subCats->has($cat->tid)): ?>
		<div class="container hide-xs">
			<div class="category-links">
				<ul>
				<?php $__currentLoopData = $subCats->get($cat->tid); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $iSubCat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<li>
						<a href="<?php echo e(\App\Helpers\UrlGen::category($iSubCat, 1)); ?>">
							<?php echo e($iSubCat->name); ?>

						</a>
					</li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
			</div>
		</div>
	<?php endif; ?>
<?php else: ?>
	<?php if(isset($cats) and !empty($cats)): ?>
		<div class="container hide-xs">
			<div class="category-links">
				<ul>
				<?php $__currentLoopData = $cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $iCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<li>
						<a href="<?php echo e(\App\Helpers\UrlGen::category($iCategory)); ?>">
							<?php echo e($iCategory->name); ?>

						</a>
					</li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
			</div>
		</div>
	<?php endif; ?>
<?php endif; ?>
<?php endif; ?><?php /**PATH /var/www/vhosts/pedidosmovil.com/clasificados1.pedidosmovil.com/resources/views/search/inc/categories.blade.php ENDPATH**/ ?>