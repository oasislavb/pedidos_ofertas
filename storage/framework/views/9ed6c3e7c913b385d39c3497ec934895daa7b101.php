
<?php if(config('settings.single.simditor_wysiwyg')): ?>
    <script src="<?php echo e(asset('assets/plugins/simditor/scripts/mobilecheck.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/simditor/scripts/module.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/simditor/scripts/uploader.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/simditor/scripts/hotkeys.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/simditor/scripts/simditor.js')); ?>"></script>
    <script type="text/javascript">
        Simditor.i18n = {
            '<?php echo e(config('app.locale')); ?>': {
                'blockquote': '<?php echo t('simditor.blockquote'); ?>',
                'bold': '<?php echo t('simditor.bold'); ?>',
                'code': '<?php echo t('simditor.code'); ?>',
                'color': '<?php echo t('simditor.color'); ?>',
                'coloredText': '<?php echo t('simditor.coloredText'); ?>',
                'hr': '<?php echo t('simditor.hr'); ?>',
                'image': '<?php echo t('simditor.image'); ?>',
                'externalImage': '<?php echo t('simditor.externalImage'); ?>',
                'uploadImage': '<?php echo t('simditor.uploadImage'); ?>',
                'uploadFailed': '<?php echo t('simditor.uploadFailed'); ?>',
                'uploadError': '<?php echo t('simditor.uploadError'); ?>',
                'imageUrl': '<?php echo t('simditor.imageUrl'); ?>',
                'imageSize': '<?php echo t('simditor.imageSize'); ?>',
                'imageAlt': '<?php echo t('simditor.imageAlt'); ?>',
                'restoreImageSize': '<?php echo t('simditor.restoreImageSize'); ?>',
                'uploading': '<?php echo t('simditor.uploading'); ?>',
                'indent': '<?php echo t('simditor.indent'); ?>',
                'outdent': '<?php echo t('simditor.outdent'); ?>',
                'italic': '<?php echo t('simditor.italic'); ?>',
                'link': '<?php echo t('simditor.link'); ?>',
                'linkText': '<?php echo t('simditor.linkText'); ?>',
                'linkUrl': '<?php echo t('simditor.linkUrl'); ?>',
                'linkTarget': '<?php echo t('simditor.linkTarget'); ?>',
                'openLinkInCurrentWindow': '<?php echo t('simditor.openLinkInCurrentWindow'); ?>',
                'openLinkInNewWindow': '<?php echo t('simditor.openLinkInNewWindow'); ?>',
                'removeLink': '<?php echo t('simditor.removeLink'); ?>',
                'ol': '<?php echo t('simditor.ol'); ?>',
                'ul': '<?php echo t('simditor.ul'); ?>',
                'strikethrough': '<?php echo t('simditor.strikethrough'); ?>',
                'table': '<?php echo t('simditor.table'); ?>',
                'deleteRow': '<?php echo t('simditor.deleteRow'); ?>',
                'insertRowAbove': '<?php echo t('simditor.insertRowAbove'); ?>',
                'insertRowBelow': '<?php echo t('simditor.insertRowBelow'); ?>',
                'deleteColumn': '<?php echo t('simditor.deleteColumn'); ?>',
                'insertColumnLeft': '<?php echo t('simditor.insertColumnLeft'); ?>',
                'insertColumnRight': '<?php echo t('simditor.insertColumnRight'); ?>',
                'deleteTable': '<?php echo t('simditor.deleteTable'); ?>',
                'title': '<?php echo t('simditor.title'); ?>',
                'normalText': '<?php echo t('simditor.normalText'); ?>',
                'underline': '<?php echo t('simditor.underline'); ?>',
                'alignment': '<?php echo t('simditor.alignment'); ?>',
                'alignCenter': '<?php echo t('simditor.alignCenter'); ?>',
                'alignLeft': '<?php echo t('simditor.alignLeft'); ?>',
                'alignRight': '<?php echo t('simditor.alignRight'); ?>',
                'selectLanguage': '<?php echo t('simditor.selectLanguage'); ?>',
                'fontScale': '<?php echo t('simditor.fontScale'); ?>',
                'fontScaleXLarge': '<?php echo t('simditor.fontScaleXLarge'); ?>',
                'fontScaleLarge': '<?php echo t('simditor.fontScaleLarge'); ?>',
                'fontScaleNormal': '<?php echo t('simditor.fontScaleNormal'); ?>',
                'fontScaleSmall': '<?php echo t('simditor.fontScaleSmall'); ?>',
                'fontScaleXSmall': '<?php echo t('simditor.fontScaleXSmall'); ?>'
            }
        };
        
        (function() {
            $(function() {
                var $preview, editor, mobileToolbar, toolbar, allowedTags;
                Simditor.locale = '<?php echo e(config('app.locale')); ?>';
                toolbar = ['bold','italic','underline','fontScale','color','|','ol','ul','blockquote','table','link'];
                mobileToolbar = ["bold", "italic", "underline", "ul", "ol"];
                if (mobilecheck()) {
                    toolbar = mobileToolbar;
                }
                allowedTags = ['br','span','a','img','b','strong','i','strike','u','font','p','ul','ol','li','blockquote','pre','h1','h2','h3','h4','hr','table'];
                editor = new Simditor({
                    textarea: $('#description'),
                    placeholder: '<?php echo e(t('Describe what makes your ad unique')); ?>...',
                    toolbar: toolbar,
                    pasteImage: false,
                    defaultImage: '<?php echo e(asset('assets/plugins/simditor/images/image.png')); ?>',
                    upload: false,
                    allowedTags: allowedTags
                });
                $preview = $('#preview');
                if ($preview.length > 0) {
                    return editor.on('valuechanged', function(e) {
                        return $preview.html(editor.getValue());
                    });
                }
            });
        }).call(this);
    </script>
<?php endif; ?>

<?php
/*


@if (!config('settings.single.simditor_wysiwyg') && config('settings.single.ckeditor_wysiwyg'))
    <script src="{{ asset('vendor/admin/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/admin/ckeditor/adapters/jquery.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            CKEDITOR.config.toolbar = [
                ['Bold','Italic','Underline','Strike','-','RemoveFormat','-','NumberedList','BulletedList','-','Undo','Redo','-','Table','-','Link','Unlink','Smiley','Source']
            ];
            $('textarea[name="description"].ckeditor').ckeditor({
                language: '{{ strtolower(ietfLangTag(config('app.locale'))) }}'
            });
        });
    </script>
@endif
*/
?>
<?php /**PATH /var/www/vhosts/pedidosmovil.com/clasificados1.pedidosmovil.com/resources/views/layouts/inc/tools/wysiwyg/js.blade.php ENDPATH**/ ?>