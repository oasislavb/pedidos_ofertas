<div class="text-center mt-4 mb-4 ml-0 mr-0">
        <button class='btn btn-fb share s_facebook'><i class="icon-facebook"></i> </button>&nbsp;
        <button class='btn btn-tw share s_twitter'><i class="icon-twitter"></i> </button>&nbsp;
        <button class='btn btn-success share s_whatsapp'><i class="fab fa-whatsapp"></i> </button>&nbsp;
        <button class='btn btn-lin share s_linkedin'><i class="icon-linkedin"></i> </button>
</div>

<?php if(false and 
(config('settings.social_link.facebook_page_url') or
config('settings.social_link.twitter_url') or
config('settings.social_link.google_plus_url') or
config('settings.social_link.linkedin_url') or
config('settings.social_link.pinterest_url') or
config('settings.social_link.instagram_url') or 
config('settings.social_link.youtube_url'))
): ?>
<div class="text-center mt-4 mb-4 ml-0 mr-0">
    <ul class="list-unstyled list-inline footer-nav social-list-footer social-list-color footer-nav-inline">
        <?php if(config('settings.social_link.facebook_page_url')): ?>
        <li>
            <a class="icon-color fb" title="" data-placement="top" data-toggle="tooltip" href="<?php echo e(config('settings.social_link.facebook_page_url')); ?>" data-original-title="Facebook">
                <i class="fab fa-facebook"></i>
            </a>
        </li>
        <?php endif; ?>
        <?php if(config('settings.social_link.twitter_url')): ?>
        <li>
            <a class="icon-color tw" title="" data-placement="top" data-toggle="tooltip" href="<?php echo e(config('settings.social_link.twitter_url')); ?>" data-original-title="Twitter">
                <i class="fab fa-twitter"></i>
            </a>
        </li>
        <?php endif; ?>
        <?php if(config('settings.social_link.instagram_url')): ?>
        <li>
            <a class="icon-color pin" title="" data-placement="top" data-toggle="tooltip" href="<?php echo e(config('settings.social_link.instagram_url')); ?>" data-original-title="Instagram">
                <i class="icon-instagram-filled"></i>
            </a>
        </li>
        <?php endif; ?>
        <?php if(config('settings.social_link.google_plus_url')): ?>
        <li>
            <a class="icon-color gp" title="" data-placement="top" data-toggle="tooltip" href="<?php echo e(config('settings.social_link.google_plus_url')); ?>" data-original-title="Google+">
                <i class="fab fa-google-plus"></i>
            </a>
        </li>
        <?php endif; ?>
        <?php if(config('settings.social_link.linkedin_url')): ?>
        <li>
            <a class="icon-color lin" title="" data-placement="top" data-toggle="tooltip" href="<?php echo e(config('settings.social_link.linkedin_url')); ?>" data-original-title="LinkedIn">
                <i class="fab fa-linkedin"></i>
            </a>
        </li>
        <?php endif; ?>
        <?php if(config('settings.social_link.pinterest_url')): ?>
        <li>
            <a class="icon-color pin" title="" data-placement="top" data-toggle="tooltip" href="<?php echo e(config('settings.social_link.pinterest_url')); ?>" data-original-title="Pinterest">
                <i class="fab fa-pinterest-p"></i>
            </a>
        </li>
        <?php endif; ?>
        <?php if(config('settings.social_link.youtube_url')): ?>
        <li>
            <a class="icon-color pin" title="" data-placement="top" data-toggle="tooltip" href="<?php echo e(config('settings.social_link.youtube_url')); ?>" data-original-title="Youtube">
                <i class="fab fa-youtube-square"></i>
            </a>
        </li>
        <?php endif; ?>
    </ul>
</div>
<?php endif; ?><?php /**PATH /var/www/vhosts/pedidosmovil.com/clasificados1.pedidosmovil.com/resources/views/layouts/inc/social/horizontal.blade.php ENDPATH**/ ?>