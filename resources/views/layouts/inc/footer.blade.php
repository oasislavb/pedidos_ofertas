<?php
if (
	config('settings.other.ios_app_url') ||
	config('settings.other.android_app_url') ||
	config('settings.social_link.facebook_page_url') ||
	config('settings.social_link.twitter_url') ||
	config('settings.social_link.google_plus_url') ||
	config('settings.social_link.linkedin_url') ||
	config('settings.social_link.pinterest_url') ||
	config('settings.social_link.instagram_url') || 
        config('settings.social_link.youtube_url')
) {
	$colClass1 = 'col-lg-3 col-md-3 col-sm-3 col-xs-6';
	$colClass2 = 'col-lg-3 col-md-3 col-sm-3 col-xs-6';
	$colClass3 = 'col-lg-2 col-md-2 col-sm-2 col-xs-12';
	$colClass4 = 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
} else {
	$colClass1 = 'col-lg-4 col-md-4 col-sm-4 col-xs-6';
	$colClass2 = 'col-lg-4 col-md-4 col-sm-4 col-xs-6';
	$colClass3 = 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
	$colClass4 = 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
}
?>
<footer>
    <div class="container">
      <ul class="row">
        <li class="col-sm-4">
          <h5>Pedidos Movil Restaurantes</h5>
          <hr>
          <p>Elige tu restaurante, categoría y platillo, accede al carrito y procede con tu compra, decide la entrega pick up o a domicilio con una tarifa extra. Finaliza tu orden especificando el método de pago si es en efectivo o electrónico y Listo tu orden esta lista !</p>
          <ul class="social_icons">            
           
            <li class="facebook"><a href="https://www.facebook.com/Pedidosmovil/" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://www.twitter.com/PedidosMovil" target="_blank"><i class="fa fa-twitter"></i></a></li>
            
            <li><a href="https://www.instagram.com/appedidosmovil/" target="_blank"><i class="fa fa-instagram"></i></a></li>
            
            <li><a href="https://www.youtube.com/channel/UCPM_-cpsvf5xk_glWVS5mKQ?view_as=subscriber" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
            
          </ul>
        </li>
        <li class="col-sm-4">
          <h5>Opinion de nuestros Usuarios ...</h5>
          <hr>
          Miles de clientes satisfechos con el servicio integral que brinda nuestro sitio, ya que sin salir del control de tu App obtienes todas las herramientas para pedir tu platillo favorito y el Delivery correspondiente hasta la comodidad de tu casa con un par de clicks!
        </li>
        <li class="col-sm-4">
          <h5>Contactanos !!!</h5>
          <hr>
          <div class="loc-info">
            <p><i class="fa fa-map-marker"></i>Durango 245, Col Roma Norte, Delegación Benito Juárez, Ciudad de México</p>
            <p><i class="fa fa-phone"></i> +52 55 5514-8289</p>             
            <p><i class="fa fa-envelope-o"></i><a href="mailto:ayuda@pedidosmovil.com">ayuda@pedidosmovil.com</a></p>
          </div>
        </li>
      </ul>
    </div>
  </footer>
  <div class="rights">
    <div class="container">
      <p class="font-montserrat">
      								
				Copyright © 2021 Pedidos Móvil Restaurantes. All Rights Reserved.
			
				  </p>
    </div>
  </div>	
