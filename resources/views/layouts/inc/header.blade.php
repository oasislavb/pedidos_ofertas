<?php
// Search parameters
$queryString = (request()->getQueryString() ? ('?' . request()->getQueryString()) : '');

// Get the Default Language
$cacheExpiration = (isset($cacheExpiration)) ? $cacheExpiration : config('settings.optimization.cache_expiration', 86400);
$defaultLang = Cache::remember('language.default', $cacheExpiration, function () {
    $defaultLang = \App\Models\Language::where('default', 1)->first();
    return $defaultLang;
});

// Check if the Multi-Countries selection is enabled
$multiCountriesIsEnabled = false;
$multiCountriesLabel = '';
if (config('settings.geo_location.country_flag_activation')) {
	if (!empty(config('country.code'))) {
		if (\App\Models\Country::where('active', 1)->count() > 1) {
			$multiCountriesIsEnabled = true;
			$multiCountriesLabel = 'title="' . t('Select a Country') . '"';
		}
	}
}

// Logo Label
$logoLabel = '';
if (getSegment(1) != trans('routes.countries')) {
	$logoLabel = config('settings.app.app_name') . ((!empty(config('country.name'))) ? ' ' . config('country.name') : '');
}

?>

<div class="top-bar">
    <div class="container">
      <ul class="left-bar-side">
        <li><p><i class="fa fa-envelope-o"></i><a href="mailto:{{config('settings.app.email')}}">{{config('settings.app.email')}}</a></p></li>
      </ul>
      <div class="dropdown d-none">
        <button class="dropbtn">@lang('restaurant.language')
          <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content ">
          <a href="{{URL::to('change_locale/en')}}">@lang('restaurant.english')</a>
          <a href="{{URL::to('change_locale/es')}}">@lang('restaurant.spanish')</a>
        </div>
      </div>
      <ul class="right-bar-side social_icons">
         <li class="facebook"><a href="{{ config('settings.social_link.facebook_page_url') }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="{{ config('settings.social_link.twitter_url') }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
           
            <li><a href="{{ config('settings.social_link.youtube_url') }}" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
      </ul>
    </div>
  </div>
  <header class="sticky">
    <div class="container">
      <div class="logo"> <a href="{{ URL::to('/') }}"><img src="https://compraventa.pedidosmovil.com/storage/app/logo/logo-5ed86d93cd947.png" alt="{{ strtolower(config('settings.app.app_name')) }}" ></a> </div>
      <nav class="animenu">
      <button class="animenu_toggle"> 
         <span class="animenu_toggle_bar"></span> 
         <span class="animenu_toggle_bar"></span> 
         <span class="animenu_toggle_bar"></span> 
      </button>
      <ul class="animenu_nav">
            <li> <a href="{{ URL::to('/') }}">Inicio</a></li>
            {{-- <li><a href="{{URL::to('restaurants')}}">@lang('restaurant.restaurants')</a></li> --}}

            @if(Auth::check() and Auth::user()->usertype=='User')

             <li> <a href="javascript:void(0);">@lang('restaurant.my_account')<i class="icon-down-open-mini"></i></a>
              <ul class="animenu_nav_child">
                <li><a href="{{ URL::to('profile') }}">@lang('restaurant.edit_profile')</a></li>
                <li><a href="{{ URL::to('change_pass') }}">@lang('restaurant.change_password')</a></li>
                <li><a href="{{URL::to('myorder')}}">@lang('restaurant.my_order')</a></li>
                <li><a href="{{ URL::to('logout') }}">@lang('restaurant.logout')</a></li>                
              </ul>
            </li>
            @elseif(Auth::check() and Auth::user()->usertype=='Owner')
              <li> <a href="javascript:void(0);">@lang('restaurant.my_account')<i class="icon-down-open-mini"></i></a>
              <ul class="animenu_nav_child">
                <li><a href="{{ URL::to('admin/dashboard') }}">@lang('restaurant.dashboard')</a></li>
                <li><a href="{{ URL::to('logout') }}">@lang('restaurant.logout')</a></li>                
              </ul>
            </li>
            @elseif(Auth::check() and Auth::user()->usertype=='Admin')
              <li> <a href="javascript:void(0);">@lang('restaurant.my_account')<i class="icon-down-open-mini"></i></a>
              <ul class="animenu_nav_child">
                <li><a href="{{ URL::to('admin/dashboard') }}">@lang('restaurant.dashboard')</a></li>
                <li><a href="{{ URL::to('logout') }}">@lang('restaurant.logout')</a></li>                
              </ul>
            </li>

              
            {{-- 
            @else
            <li><a href="{{ URL::to('login') }}">Login</a></li>
            <li><a href="{{ URL::to('register') }}">Register</a></li>
            --}}

            @endif

            {{-- <li><a href="{{ URL::to('about') }}">Sobre nosotros</a></li> --}}
            <li><a href="https://pedidosmovil.com">Sobre nosotros</a></li>
            {{-- <li><a href="{{ URL::to('contact') }}">Contáctenos</a></li>               --}}
          </ul>
       
       
    </nav>
    </div>
  </header>
   